export class AddProduct {

  id: number | undefined;
  name: string | undefined;
  wtPrice: number | undefined;
  taxInclPrice: number | undefined;
  description: string | undefined;
  categoryId: number | undefined;

  constructor(name: string, wtPrice: number, taxInclPrice: number, description: string, categoryId: number) {
    this.name = name;
    this.wtPrice = wtPrice;
    this.taxInclPrice = taxInclPrice;
    this.description = description;
    this.categoryId = categoryId;
  }

}
