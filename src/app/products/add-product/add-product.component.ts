import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProductService } from "../service/product.service";
import {AddProduct} from "../interface/addProduct";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  checkoutForm = this.formBuilder.group({
    name: '',
    wtPrice: '',
    taxInclPrice: '',
    description: '',
    categoryId: ''
  });
  constructor(private router: Router, private formBuilder: FormBuilder, private productService: ProductService) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
      this.productService.addProduct(this.checkoutForm.value)
        .subscribe(() => this.router.navigate(['/products']));
  }

}
