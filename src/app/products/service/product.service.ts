import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../interface/product";
import {environment} from "../../../environments/environment";
import {AddProduct} from "../interface/addProduct";

const productUrl = environment.apiUrl + "/products";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) {}

  getProducts() {
    return this.http.get<Product[]>(productUrl);
  }

  addProduct(value: any) {
    return this.http.post<AddProduct[]>(productUrl, {
      name: value.name,
      wtPrice: value.wtPrice,
      taxInclPrice: value.taxInclPrice,
      description: value.description,
      categoryDto: {
        id: value.categoryId
      }
    });
  }

  updateProduct(id: number, value: any) {
    console.log(id)
    return this.http.put(productUrl + '/' + id, {
      id: value.id,
      name: value.name,
      wtPrice: value.wtPrice,
      taxInclPrice: value.taxInclPrice,
      description: value.description,
      categoryDto: {
        id: value.categoryId
      }
    });
  }

  deleteProduct(id: number) {
    return this.http.delete(productUrl + '/' + id);
  }
}
