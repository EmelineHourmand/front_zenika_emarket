import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../interface/product";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../service/product.service";
import {map} from "rxjs/operators";
import {FormBuilder} from "@angular/forms";
import {AddProduct} from "../interface/addProduct";

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  product!: Observable<Product | undefined>;
  idProduct = this.getIdProduct();
  checkoutForm = this.formBuilder.group({
    id: '',
    name: '',
    wtPrice: '',
    taxInclPrice: '',
    description: '',
    categoryId: ''
  });

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private  productService: ProductService) { }

  ngOnInit(): void {
    this.product = this.productService.getProducts()
      .pipe(map(products => products.find(p => p.id === this.idProduct)));

    this.product.subscribe(res => {
      console.log(res)
      const p :AddProduct = {
        id: this.idProduct,
        name: res?.name,
        wtPrice: undefined,
        taxInclPrice: res?.price,
        description: res?.description,
        categoryId: res?.categoryId
      }
      this.checkoutForm.patchValue(p || {})
    } );


  }

  getIdProduct() {
    const routeParams = this.route.snapshot.paramMap;
    return Number(routeParams.get('productId'));
  }

  onSubmit() {
    console.log(this.checkoutForm.value)
    this.productService.updateProduct(this.idProduct, this.checkoutForm.value).subscribe();
  }

}

