import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../interface/product";
import {ActivatedRoute} from "@angular/router";
import {ProductService} from "../service/product.service";
import {find, map} from "rxjs/operators";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  product!: Observable<Product | undefined>;
  idProduct = this.getIdProduct();

  constructor(private route: ActivatedRoute, private  productService: ProductService) { }

  ngOnInit(): void {
    const productIdFromRoute = this.getIdProduct();

    this.product = this.productService.getProducts()
      .pipe(map(products => products.find(p => p.id === productIdFromRoute)));
    this.product.subscribe(res => console.log(res));
  }

  getIdProduct() {
    const routeParams = this.route.snapshot.paramMap;
    return Number(routeParams.get('productId'));
  }

  onDelete(): void {
    this.productService.deleteProduct(this.idProduct).subscribe();
  }

}
