import { Component, OnInit } from '@angular/core';
import {Product} from "../interface/product";
import {Observable} from "rxjs";
import {ProductService} from "../service/product.service";

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  products: Observable<Product[]> | undefined;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.products = this.productService.getProducts();
  }

  onDelete(idProduct: number): void {
    this.productService.deleteProduct(idProduct).subscribe();
  }

}
