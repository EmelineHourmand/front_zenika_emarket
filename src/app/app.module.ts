import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ListProductsComponent } from './products/list_products/list-products.component';
import { ProductDetailsComponent } from './products/product_details/product-details.component';
import {HttpClientModule} from "@angular/common/http";
import { HomeComponent } from './index/home/home.component';
import { HeaderComponent } from './index/header/header.component';
import { FooterComponent } from './index/footer/footer.component';
import {RouterModule} from "@angular/router";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AddProductComponent } from './products/add-product/add-product.component';
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import { UpdateProductComponent } from './products/update-product/update-product.component';

@NgModule({
  declarations: [
    AppComponent,
    ListProductsComponent,
    ProductDetailsComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AddProductComponent,
    UpdateProductComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatIconModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent},
      { path: 'products', component: ListProductsComponent},
      { path: 'products/add', component: AddProductComponent},
      { path: 'products/update/:productId', component: UpdateProductComponent},
      { path: 'products/:productId', component: ProductDetailsComponent}
    ]),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
